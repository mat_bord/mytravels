-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 09, 2019 alle 10:18
-- Versione del server: 5.6.33-log
-- PHP Version: 5.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `my_gafliceolevi`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `Itinerario`
--

CREATE TABLE IF NOT EXISTS `Itinerario` (
  `Id` int(16) NOT NULL AUTO_INCREMENT,
  `Luogo` varchar(64) NOT NULL,
  `Nome` varchar(64) NOT NULL,
  `Creatore` varchar(64) NOT NULL,
  `Durata_giorni` smallint(2) NOT NULL DEFAULT '0',
  `Durata_ore` smallint(2) DEFAULT '0',
  `Costo` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `Creatore` (`Creatore`),
  KEY `Luogo` (`Luogo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dump dei dati per la tabella `Itinerario`
--

INSERT INTO `Itinerario` (`Id`, `Luogo`, `Nome`, `Creatore`, `Durata_giorni`, `Durata_ore`, `Costo`) VALUES
(24, 'Ciao', 'H', 'squeri', 23, 10, 50),
(23, 'Europa', 'Interrail', 'zack', 5, 10, 500),
(22, '0', '0', 'squeri', 0, 0, 0),
(21, 'cittanno', 'viaggietto a cittanno', 'squeri', 1, 1, 4999);

-- --------------------------------------------------------

--
-- Struttura della tabella `Tappa`
--

CREATE TABLE IF NOT EXISTS `Tappa` (
  `Numero` int(16) NOT NULL AUTO_INCREMENT,
  `Itinerario` int(16) NOT NULL,
  `Voto` float NOT NULL,
  `Commento` varchar(512) NOT NULL,
  `Nome` varchar(64) NOT NULL,
  `Latitudine` decimal(10,8) NOT NULL,
  `Longitudine` decimal(11,8) NOT NULL,
  `Indirizzo` varchar(256) NOT NULL,
  PRIMARY KEY (`Numero`,`Itinerario`),
  KEY `Itinerario` (`Itinerario`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dump dei dati per la tabella `Tappa`
--

INSERT INTO `Tappa` (`Numero`, `Itinerario`, `Voto`, `Commento`, `Nome`, `Latitudine`, `Longitudine`, `Indirizzo`) VALUES
(14, 21, 5, 'mmh che bella la villozza di ricca', 'Civitanova Marche', 43.30484920, 13.72183510, '62012 Civitanova Marche MC, Italia'),
(15, 21, 4, 'Bello', 'Casa della Poesia', 43.72522717, 12.63786077, 'Via Valerio, 3, 61029 Urbino PU, Italia'),
(16, 21, 4, 'hbh', 'yu', 42.35233080, 12.97687565, 'Unnamed Road, 02015 Cittaducale RI, Italia');

-- --------------------------------------------------------

--
-- Struttura della tabella `Utente`
--

CREATE TABLE IF NOT EXISTS `Utente` (
  `Username` varchar(16) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Nome` varchar(64) DEFAULT NULL,
  `Cognome` varchar(64) DEFAULT NULL,
  `Mail` varchar(64) NOT NULL,
  `Data_nascita` date DEFAULT NULL,
  `Sesso` varchar(7) DEFAULT NULL,
  `Reputazione` int(16) DEFAULT NULL,
  PRIMARY KEY (`Username`),
  UNIQUE KEY `Mail` (`Mail`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `Utente`
--

INSERT INTO `Utente` (`Username`, `Password`, `Nome`, `Cognome`, `Mail`, `Data_nascita`, `Sesso`, `Reputazione`) VALUES
('squeri', '$2y$10$3PSYja42Y3/9wzXEP4GR8.2Qor9iqmBRODdG3fe2O3GKiojrqw9Qu', 'good boye', '*mlem*', 'goodboye97@gmail.com', '0001-01-01', 'Altro', 0),
('fakebordo', '$2y$10$yNLgEb2ycX7pvzQ91kYsD.N0CqvHnErZhyLKyEkvSPyCNtcN8O826', 'bordo', 'fake', 'fakebordo@gmail.com', '2019-02-04', NULL, 0),
('zack', '$2y$10$eMu7QCNwN3tgPBgPmWEYeOFdcKHjiQ0xdcAfnBkLwSDVQCvSglpq6', 'Matteo', 'Bordin', 'ciao@gmail.com', '1997-02-19', 'Maschio', 0),
('Piaz97', '$2y$10$6ixnURjpWrr2zy72zfhVd.QJj2eBY1WKXLFZXpfXCAk8fr52SjJey', 'Samuele Giuliano', 'Piazzetta', 'samuele.piazzetta@gmail.com', '1997-08-09', 'Maschio', 0),
('gian', '$2y$10$/Qfuxb6e4R0EFFZ5fXRXH.DPJvCAYVXBNWQ56v2Jo4K9ilwOlKCCC', NULL, NULL, 'asd@gmaiasdasdasd.com', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `Valutazione`
--

CREATE TABLE IF NOT EXISTS `Valutazione` (
  `Utente` varchar(16) NOT NULL,
  `Itinerario` int(16) NOT NULL,
  `Voto` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`Utente`,`Itinerario`),
  KEY `Itinerario` (`Itinerario`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Trigger `Valutazione`
--
DROP TRIGGER IF EXISTS `reputazioneUtente`;
DELIMITER //
CREATE TRIGGER `reputazioneUtente` AFTER INSERT ON `Valutazione`
 FOR EACH ROW UPDATE Utente
    SET Reputazione = Reputazione+new.Voto
    WHERE username = (SELECT Creatore FROM Itinerario INNER JOIN Valutazione ON Itinerario.Id=new.Itinerario GROUP BY Creatore)
//
DELIMITER ;
DROP TRIGGER IF EXISTS `reputazioneUtenteCancellazione`;
DELIMITER //
CREATE TRIGGER `reputazioneUtenteCancellazione` BEFORE DELETE ON `Valutazione`
 FOR EACH ROW UPDATE Utente
    SET Reputazione = Reputazione-old.Voto
    WHERE username = (SELECT Creatore FROM Itinerario INNER JOIN Valutazione ON Itinerario.Id=old.Itinerario GROUP BY Creatore)
//
DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
