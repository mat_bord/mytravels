<div id="profileContainer">
    <?php
    include_once("include/profileNavbar.php");
    ?>
    <div id="rightTravels">
        <nav id="breadcrumb" aria-label="Breadcrumbs">
            <span>Ti trovi in:&nbsp;</span>
            <ol>
                <li><a href="area_personale.php">Area personale</a></li>
                <li>I miei coupon</li>
            </ol>
        </nav>
        <?php
            include_once("funzioni.php");
            stampaCoupon($_SESSION['username']);
        ?>
    </div>
</div>