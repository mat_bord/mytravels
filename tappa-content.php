<div class="LuogoContainer">
    <div class="LuogoTitoloContainer">
        <h1 id="LuogoTitolo">Aggiungi una tappa al tuo viaggio!</h1>
        <span id="LuogoClose"><a href="miei_viaggi.php">x</a></span>
    </div>
    <div class="PlaceCenterContainer">
        <div class="PlaceFormContainer">
            <form id="PlaceForm" action="aggiungi-tappa.php" method="post" enctype="multipart/form-data">
                <div class="PlaceInputBox">
                    <label for="PlaceName" class="sr-only">Aggiungi un nome a questo luogo</label>
                    <input class="PlaceInput" type="text" id="PlaceName" placeholder="Aggiungi un nome a questo luogo!" name="Name" maxlength="64" required="required"/>
                </div>
                <div class="PlaceTextArea">
                    <label for="PlaceDesc" class="sr-only">Aggiungi una descrizione a questo luogo</label>
                    <textarea id="PlaceDesc" placeholder="Aggiungi una descrizione del luogo" name="Description" cols="20" rows="5" maxlength="512" required="required"></textarea>
                </div>
                <div id="PlaceRatingBox">
                    <p>Che voto dai a questo luogo?</p>
                    <div class="PlaceRate">
                        <fieldset>
                            <legend class="sr-only">stelle per la valutazione della tappa</legend>
                            <input class="sr-only" type="radio" id="star5" name="Evaluation" value="5" required="required"/>
                            <label for="star5" title="text">5 stars</label>
                            <input class="sr-only" type="radio" id="star4" name="Evaluation" value="4" required="required"/>
                            <label for="star4" title="text">4 stars</label>
                            <input class="sr-only" type="radio" id="star3" name="Evaluation" value="3" required="required"/>
                            <label for="star3" title="text">3 stars</label>
                            <input class="sr-only" type="radio" id="star2" name="Evaluation" value="2" required="required"/>
                            <label for="star2" title="text">2 stars</label>
                            <input class="sr-only" type="radio" id="star1" name="Evaluation" value="1" required="required"/>
                            <label for="star1" title="text">1 star</label>
                        </fieldset>

                    </div>
                </div>
                <div class="PlaceFileBox">
                    <p id="FileName">Aggiungi una foto del posto!</p>
                    <label class ="materialButton flexButton _50" for="PlaceImg">Seleziona</label>
                    <input class="sr-only" type="file" id="PlaceImg"  name="Image" accept="image/*" onchange="loadFile(event)" required="required"/>

                    <p id="anteprimaFoto">Anteprima</p>
                    <div id="output"></div>

                    <?php session_start();
                    if ($_SESSION["errorStopPicture_1"] == true){
                        echo ("<p class='danger'>Formato non valido. Carica una foto in JPG o PNG.</p>");
                        $_SESSION['errorStopPicture_1'] = false;
                    }
                    else if ($_SESSION["errorStopPicture_2"] == true){
                        echo ("<p class='danger'>L'immagine è oltre le dimensioni massime consentite. Carica una foto inferiore a 8MB.</p>");
                        $_SESSION['errorStopPicture_2'] = false;
                    }
                    else if ($_SESSION["errorStopPicture_3"] == true){
                        echo ("<p class='danger'>Immagine non selezionata. Carica una foto in JPG o PNG.</p>");
                        $_SESSION['errorStopPicture_2'] = false;
                    }
                    else if ($_SESSION["errorStopPicture"] == true){
                        echo ("<p class='danger'>C'è stato un errore con il caricamento dell'immagine, ti invitiamo a riprovare.</p>");
                        $_SESSION['errorStopPicture'] = false;
                    }
                    else if ($_SESSION["errorTappaBianco"] == true){
                        echo ("<p class='danger'>La descrizione e il nome della tappa non possono rimanere vuoti.</p>");
                        $_SESSION['errorTappaBianco'] = false;
                    }
                    ?>
                </div>
                <hr id="PlaceFormSeparator"/>
                <div>

                    <input type="hidden" id="PlaceAddress"  name="Address"/>
                    <input type="hidden" id="PlaceLatitude" name="Latitude"/>
                    <input type="hidden" id="PlaceLongitude" name="Longitude"/>
                    <?php session_start();
                    if ($_SESSION["errorPlace"] == true){
                        echo ("<p class='danger'>Luogo non inserito, posiziona un marker sulla mappa o sceglilo dalla barra di ricerca</p>");
                        $_SESSION["errorPlace"] = false;
                    }
                    ?>
                </div>
                <div class="PlaceButtonContainer">
                    <button type="submit" class="materialButton _50">Aggiungi Tappa</button>
                </div>
            </form>
        </div>
    </div>

    <div class="MapContainer">

        <label class="sr-only" for="pac-input">Mappa</label>

        <input id="pac-input" class="controls" type="text" placeholder="Search Box"/>
        <noscript>
            <div id="notLoggedContainer">
                Sembra che tu abbia JavaScript disabilititato. È necessario attivarlo per poter utilizzare la mappa
            </div>
        </noscript>
        <div id="map"></div>

    </div>
</div>